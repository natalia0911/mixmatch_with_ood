import pandas as pd
import torch
import time
import numpy as np
import torchvision.models as models3
from scipy.stats import entropy
from scipy.spatial import distance
import matplotlib
import matplotlib.pyplot as plt

NUM_BINS = 15
#Takes the previous csv files stored during the calculation of the uncertainties for each method, to estimate its reliability


def calculate_stats(results_folder, base_file_name ="/CHINA_BATCH_MCD_$#_uncertainty_wrong_correct.csv", num_batches = 10, number_labels = 20):
    """
    Calculates the stats for the given csv file
    :param results_folder: folder with the csvs
    :param base_file_name: base file name to further customize
    :param num_batches: number of batches
    :param number_labels: number of labels
    :return:
    """
    maxes = []
    mins = []
    for i in range(0, 2):
        if(i == 0):
            base_file_name_to_use = base_file_name.replace("#", str("ssdl"))
            print("Is SSDL")
        elif (i == 1):
            base_file_name_to_use = base_file_name.replace("#", str("no_ssdl"))
            print("Is NO SSDL")

        results_folder = results_folder.replace("$", str(number_labels))
        correct_list_all = []
        wrong_list_all = []
        for i in range(0, num_batches):
            base_file_name_i = base_file_name_to_use.replace("$", str(i))
            print("file name ", base_file_name_i)
            results_file = results_folder + base_file_name_i
            data = pd.read_csv(results_file)
            #the two last elements are the mean and std already
            correct_list = remove_nans(data.iloc[[0]].values.tolist()[0][1:])[:-2]
            correct_list_all += correct_list
            wrong_list = remove_nans(data.iloc[[1]].values.tolist()[0][1:])[:-2]
            wrong_list_all += wrong_list

        #compute all the stats given the files
        wrong_tensor_all_tensor_ssdl = torch.tensor(wrong_list_all)
        correct_tensor_all_tensor_ssdl = torch.tensor(correct_list_all)
        print("Total elements wrong: ", wrong_tensor_all_tensor_ssdl.shape)
        print("Total elements correct: ", correct_tensor_all_tensor_ssdl.shape)
        print("Stats for model with number of labels ", number_labels)
        print("Mean wrong: ", torch.mean(wrong_tensor_all_tensor_ssdl))
        print("Mean correct: ", torch.mean(correct_tensor_all_tensor_ssdl))
        print("Std wrong: ", torch.std(wrong_tensor_all_tensor_ssdl))
        print("Std correct: ", torch.std(correct_tensor_all_tensor_ssdl))
        max_range = max(torch.max(correct_tensor_all_tensor_ssdl).item(), torch.max(wrong_tensor_all_tensor_ssdl).item())
        min_range = max(torch.min(correct_tensor_all_tensor_ssdl).item(), torch.min(wrong_tensor_all_tensor_ssdl).item())
        maxes += [max_range]
        mins += [min_range]
    #use the same ranges for both ssdl and no ssdl, to make use of a comparable pdf distance
    max_range = max(maxes)
    min_range = min(mins)
    #to calculate the jensen shannon distance
    for i in range(0, 2):
        if (i == 0):
            base_file_name_to_use = base_file_name.replace("#", str("ssdl"))
            print("Is SSDL")
        elif(i == 1):
            base_file_name_to_use = base_file_name.replace("#", str("no_ssdl"))
            print("Is NO SSDL")

        results_folder = results_folder.replace("$", str(number_labels))
        correct_list_all = []
        wrong_list_all = []
        for j in range(0, num_batches):
            base_file_name_i = base_file_name_to_use.replace("$", str(j))
            #print("file name ", base_file_name_i)
            results_file = results_folder + base_file_name_i
            #print("file name ", results_file)
            data = pd.read_csv(results_file)
            # the two last elements are the mean and std already
            correct_list = remove_nans(data.iloc[[0]].values.tolist()[0][1:])[:-2]
            correct_list_all += correct_list
            wrong_list = remove_nans(data.iloc[[1]].values.tolist()[0][1:])[:-2]
            wrong_list_all += wrong_list

        # compute all the stats given the files
        if(i == 0):
            wrong_tensor_all_tensor_ssdl = torch.tensor(wrong_list_all)
            correct_tensor_all_tensor_ssdl = torch.tensor(correct_list_all)
            js_dist, hist1_correct_ssdl, hist2_wrong_ssdl, bucks1_correct_ssdl, bucks2_wrong = calculate_js_dist(max_range, min_range, torch.tensor(correct_list_all), torch.tensor(wrong_list_all), distance_func=distance.jensenshannon)
        elif (i == 1):
            wrong_tensor_all_tensor_no_ssdl = torch.tensor(wrong_list_all)
            correct_tensor_all_tensor_no_ssdl = torch.tensor(correct_list_all)
            js_dist, hist1_correct_no_ssdl, hist2_wrong_no_ssdl, bucks1_correct_no_ssdl, bucks2_wrong = calculate_js_dist(max_range, min_range,torch.tensor(correct_list_all), torch.tensor(wrong_list_all), distance_func=distance.jensenshannon)


    print("Jensen shannon distance: ", js_dist)
    print("HIST 1 NO SSDL CORRECT")
    print(hist1_correct_no_ssdl)
    print("HIST 1  SSDL CORRECT")
    print(hist1_correct_ssdl)
    return correct_tensor_all_tensor_ssdl, wrong_tensor_all_tensor_ssdl, correct_tensor_all_tensor_no_ssdl, wrong_tensor_all_tensor_no_ssdl, js_dist, hist1_correct_ssdl, hist2_wrong_ssdl, bucks1_correct_ssdl, hist1_correct_no_ssdl, hist2_wrong_no_ssdl, bucks1_correct_no_ssdl

def calculate_js_dist(max_range, min_range, tensor_results_correct, tensor_results_wrong, distance_func=distance.jensenshannon):
    """
    Calculates the Jensen Shannon distance between the distributions of the uncertainties
    :param max_range: maximum value of the uncertainties
    :param min_range: minimum value of the uncertainties
    :param tensor_results_correct: tensor with the uncertantities of the correct estimations
    :param tensor_results_wrong: tensor with the uncertantities of the incorrect estimations
    :param distance_func: specific distance function, another one can be used
    :return:
    """
    print("computed range ", (min_range, max_range))

    (hist1, bucks1) = np.histogram(tensor_results_correct.numpy(), bins=NUM_BINS, range=(min_range, max_range), normed=None, weights=None,
                                   density=None)
    print("hist1 ", hist1)
    print("buckets 1", bucks1)
    # ensure that the histograms have the same meaning, by using the same buckets
    (hist2, bucks2) = np.histogram(tensor_results_wrong.numpy(), bins=bucks1, range=None, normed=None, weights=None,
                                   density=None)

    print("hist2 ", hist2)
    print("buckets 2", bucks2)
    print("Num wrongs ", tensor_results_wrong.shape, " sum hist2 ", hist2.sum())
    # normalize the histograms
    hist1 = np.array(hist1) / sum(hist1)
    hist2 = np.array(hist2) / sum(hist2)
    # ent = entropy(hist1.tolist(), qk = hist2.tolist())
    js_dist = distance_func(hist1.tolist(), hist2.tolist())
    return js_dist, hist1, hist2, bucks1, bucks2

def remove_nans(list_nans):
    """
    Removes the nans in the given list
    :param list_nans:
    :return:
    """
    cleaned_list = [x for x in list_nans if str(x) != 'nan']
    return cleaned_list



def plot_pair_functions(x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, y_plot_acc_no_ssdl_std, x_plot_uncertainty_ssdl, y_plot_acc_ssdl, y_plot_acc_ssdl_std, plot_name):
    """
    Plots a pair of functions
    :param x_plot_uncertainty_no_ssdl: x of uncertainty plot, ssdl model
    :param y_plot_acc_no_ssdl: y of uncertainty plot, no ssld model
    :param y_plot_acc_no_ssdl_std:  y of accuracy, no ssdl standard deviation
    :param x_plot_uncertainty_ssdl: x of uncertainty plot, ssdl model
    :param y_plot_acc_ssdl: y of plot for the accuracy of a ssdl model
    :param y_plot_acc_ssdl_std: y of plot for the accuracy of ssdl model
    :param plot_name:
    :return:
    """
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
        'font.size': 22
    })
    fig, ax = plt.subplots()
    ax.plot(x_plot_uncertainty_no_ssdl, y_plot_acc_no_ssdl, '--')
    ax.fill_between(x_plot_uncertainty_no_ssdl, np.array(y_plot_acc_no_ssdl) - np.array(y_plot_acc_no_ssdl_std), np.array(y_plot_acc_no_ssdl) + np.array(y_plot_acc_no_ssdl_std), alpha = 0.2, edgecolor='#089FFF', facecolor='#089FFF')
    ax.plot(x_plot_uncertainty_ssdl, y_plot_acc_ssdl, 'x-')
    ax.fill_between(x_plot_uncertainty_ssdl, np.array(y_plot_acc_ssdl) - np.array(y_plot_acc_ssdl_std),
                    np.array(y_plot_acc_ssdl) + np.array(y_plot_acc_ssdl_std), alpha=0.1, edgecolor='#FFA500', facecolor='#FFA500')
    #ax.plot(x_plot_uncertainty_ssdl, np.array(x_plot_uncertainty_ssdl)/100, '-d')

    if("softmax" in plot_name):
        ax.set_xlabel('Softmax confidence percentile (\%)')
    elif("mcd" in plot_name):
        ax.set_xlabel('MCD uncertainty percentile (\%)')
    elif("duq" in plot_name):
        ax.set_xlabel('DUQ uncertainty percentile (\%)')

    ax.set_ylabel('Mean accuracy per bin')
    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()
    plt.savefig(plot_name, dpi=400)
    print("std list")
    print(y_plot_acc_no_ssdl_std)


def calculate_percentiles(correct_tensor_all_tensor, wrong_tensor_all_tensor):
    """
    Calculates the percentiles for the given tensors (Calculation of ECE)
    :param correct_tensor_all_tensor: tensor with correct estimations
    :param wrong_tensor_all_tensor: tensor with wrong estimations
    :return:
    """
    y_hits = torch.cat((torch.ones(correct_tensor_all_tensor.shape), torch.zeros(wrong_tensor_all_tensor.shape)),0 )
    x_uncertainties = torch.cat((correct_tensor_all_tensor,wrong_tensor_all_tensor), 0)

    perc_uncertainty = 0
    perc_uncertainty_prev = 0
    x_plot_uncertainty = []
    y_plot_acc = []
    y_plot_acc_std = []
    for i in range(1, 10):
        percentile = i * 10

        perc_uncertainty = np.percentile(x_uncertainties.numpy(), percentile)
        print("Perc uncertainty")
        print(perc_uncertainty)
        print("y_hits")
        print(y_hits.shape)
        y_hits_percentile = y_hits[x_uncertainties < perc_uncertainty]
        x_uncertainties_new = x_uncertainties[x_uncertainties < perc_uncertainty]
        y_hits_percentile = y_hits_percentile[x_uncertainties_new > perc_uncertainty_prev]

        print("y_hits_percentile")
        print(y_hits_percentile.sum())
        x_plot_uncertainty += [percentile]
        y_plot_acc += [y_hits_percentile.mean()]
        y_plot_acc_std += [y_hits_percentile.numpy().std()]

        perc_uncertainty_prev = perc_uncertainty


    return x_plot_uncertainty, y_plot_acc, y_plot_acc_std


def plot_histograms(bins1, y1, bins2, y2, plot_name = "histogram.pgf"):
    """
    Histogram plotter in latex, saves the plot to latex
    :param bins1:
    :param y1:
    :param bins2:
    :param y2:
    :param plot_name:
    :param title_plot:
    :return:
    """
    #matplotlib.use("pgf")
    matplotlib.rcParams.update({
        "pgf.texsystem": "pdflatex",
        'font.family': 'serif',
        'text.usetex': True,
        'pgf.rcfonts': False,
        'font.size': 20
    })
    print("hist1 ")
    print(y1.shape)
    print("buckets ")
    print(bins1[0:-1].shape)
    fig, ax = plt.subplots()
    ax.plot(bins1[0:-1], y1/y1.sum(), '--')
    ax.plot(bins2[0:-1], y2 / y2.sum(), '-x')
    ax.set_xlabel('Confidence measure')
    ax.set_ylabel('Probability density')
    #ax.set_title(title_plot)

    # Tweak spacing to prevent clipping of ylabel
    fig.tight_layout()

    plt.savefig(plot_name, dpi=400)


