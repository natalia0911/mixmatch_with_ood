declare -a batches=("0" "1" "2" "3" "4" "5" "6" "7" "8" "9")
declare -a path_dest_arr=("/media/Data/saul/Datasets/OOD_COVID_19_FINAL_TESTS/UNLABELED/INDIANA_65_CR_35_No_Preprocessed/")
declare -a path_ood_arr=("/media/Data/saul/Datasets/Covid19/Sources/NORMAL_CR_DATASET_with_letters/")
declare -a path_iod_arr=("/media/Data/saul/Datasets/Covid19/Sources/all_INDIANA")

#Create unlabeled data with contamination
for ((j=0;j<${#batches[@]};++j)); do
  python ../utilities/dataset_partitioner.py --mode unlabeled_train_test_partitioner --path_ood ${path_ood_arr[0]} --path_iod ${path_iod_arr[0]} --path_dest   ${path_dest_arr[0]} --ood_perc 0.35 --num_unlabeled 90 --batch_id_num ${batches[j]} --num_labeled 20 --num_test 100 --num_unlabeled_total 90
done