#remember to use   dos2unix
declare -a batches=("batch_0" "batch_1" "batch_2" "batch_3"  "batch_4" "batch_5" "batch_6" "batch_7" "batch_8" "batch_9") #...
# declare -a batches=("batch_0") #...

declare -a num_labeled=("130" )
declare -a epochs=("50")

declare -a    num_unlabeled=("150")
#5 is true
declare -a balanced_flag=("5" "-1")
#declare -a ood_perc=("66")
declare -a lambda=("200")
declare -a mode=("ssdl" "partial_supervised")
declare -a modeL=("densenet")
declare -a imbalance=("10_45_45")
declare -a path_labeled=("/media/Data/saul/Datasets/Covid19/Dataset/VALENCIA_COHEN_RSNA_Multiclass/batches_labeled_undersampled")


for ((i=0;i<${#num_labeled[@]};++i)); do
	for ((j=0;j<${#batches[@]};++j)); do
	    python ../MixMatch_OOD_main_balance_control_COVID.py --K_transforms 2 --T_sharpening 0.25 --alpha_mix 0.75 --balanced ${balanced_flag[0]} --batch_size 10 --epochs ${epochs[0]} --lambda_unsupervised ${lambda[0]} --log_folder logs_ASOC/logs_COVID_SSDL_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_balanced_${balanced_flag[0]} --lr 0.00002 --mode ${mode[0]} --model "densenet"  --num_classes 3 --number_labeled ${num_labeled[i]} --path_labeled  ${path_labeled[0]}/${batches[j]}  --results_file_name stats_${mode[0]}_supervised_COVID_MULTIC_ASOC_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_BALANCED_${balanced_flag[0]}_${imbalance[0]}.csv --size_image 220 --weight_decay 0.0001 --desired_labeled_classes_dist "0.45, 0.45, 0.1"
	    python ../MixMatch_OOD_main_balance_control_COVID.py --K_transforms 2 --T_sharpening 0.25 --alpha_mix 0.75 --balanced ${balanced_flag[1]} --batch_size 10 --epochs ${epochs[0]} --lambda_unsupervised ${lambda[0]} --log_folder logs_ASOC/logs_COVID_SSDL_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_balanced_${balanced_flag[1]} --lr 0.00002 --mode ${mode[0]} --model "densenet" --num_classes 3 --number_labeled ${num_labeled[i]} --path_labeled  ${path_labeled[0]}/${batches[j]}  --results_file_name stats_${mode[0]}_supervised_COVID_MULTIC_ASOC_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_BALANCED_${balanced_flag[1]}_${imbalance[0]}.csv --size_image 220 --weight_decay 0.0001 --desired_labeled_classes_dist "0.45, 0.45, 0.1"
	    python ../MixMatch_OOD_main_balance_control_COVID.py --K_transforms 2 --T_sharpening 0.25 --alpha_mix 0.75 --balanced ${balanced_flag[0]} --batch_size 10 --epochs ${epochs[0]} --lambda_unsupervised ${lambda[0]} --log_folder logs_ASOC/logs_COVID_SSDL_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_balanced_${balanced_flag[0]} --lr 0.00002 --mode ${mode[1]} --model "densenet"  --num_classes 3 --number_labeled ${num_labeled[i]} --path_labeled ${path_labeled[0]}/${batches[j]}  --results_file_name stats_${mode[1]}_supervised_COVID_MULTIC_ASOC_${num_labeled[i]}_unlabeled_${num_unlabeled[0]}_BALANCED_${balanced_flag[0]}_${imbalance[0]}.csv --size_image 220 --weight_decay 0.0001 --desired_labeled_classes_dist "0.45, 0.45, 0.1"
	done
done
