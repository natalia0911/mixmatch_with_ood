import PIL
import os
import os.path
from PIL import Image
import numpy as np
import SimpleITK as sitk
import cv2
from cv2_rolling_ball import subtract_background_rolling_ball

#Script for applying preprocessing pipeline to mammogram images
#Adapted from implementation made available at https://github.com/ABRB13/BreastCancer
#by authors of A. R. Beeravolu, S. Azam, M. Jonkman, B. Shanmugam, K. Kannoorpatti and A. Anwar, "Preprocessing of Breast Cancer Images to Create Datasets for Deep-CNN," in IEEE Access, vol. 9, pp. 33438-33463, 2021, doi: 10.1109/ACCESS.2021.3058773.

#This same script is also supplied in the zip file "cbis_ddsm_224px"



def apply_morphological_transformations(binary_map):
    kernel_size = 35 #Can be adjusted to experiment with background removal
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    erosion = cv2.erode(binary_map, kernel, iterations=1)
    kernel = np.ones((kernel_size, kernel_size), np.uint8)
    dilation = cv2.dilate(erosion, kernel, iterations=1)

    return dilation


def apply_binary_map(img, binary_map):
    merged = cv2.bitwise_and(img, binary_map)

    return merged


def preprocess_image(path, image=None):
    if (image is None):
        img = cv2.imread(path, 0)
    else:
        img = image.copy()
        
        
    # Rolling ball
    artifacts, background = subtract_background_rolling_ball(img, 5, light_background=True,
                                                             use_paraboloid=False, do_presmooth=True)
    # Huang thresholding
    filter = sitk.HuangThresholdImageFilter()
    filter.SetInsideValue(0)
    filter.SetOutsideValue(255)
    binary_map = filter.Execute(sitk.GetImageFromArray(background))
    binary_map = sitk.GetArrayFromImage(binary_map)

    binary_map = apply_morphological_transformations(binary_map)

    if image is None:
        img = cv2.imread(path, 0)
    else:
        img = image.copy()

    result = apply_binary_map(img, binary_map)

    return result

#Apply "background cleaning" to all images in subdirs of a specified directory.
#Operations happen IN PLACE, so existing images are overwritten 
if __name__ == '__main__':

    count = 0
    source_path = 'all_cleaned' #root path of subdirs with images
    img_dirs = os.listdir(source_path)
    for directory in img_dirs:
        for subdir, dirs, files in os.walk(os.path.join(source_path, directory)):
            for file in files:
                count += 1
            
                f_img = os.path.join(subdir, file)
                
                result = preprocess_image(f_img)

                cv2.imwrite(f_img, result)
                
                print(f_img, " cleaned")

    

#Implementations:
# Rolling ball from cv2
# Huang, https://pypi.org/project/SimpleITK/
