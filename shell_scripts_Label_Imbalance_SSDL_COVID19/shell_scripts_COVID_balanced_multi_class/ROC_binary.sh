#remember  to use     dos2unix
declare  -a batches=("batch_0")
#declare -a batches=("batch_0" "batch_1" "batch_2" "batch_3" "batch_4" "batch_5" "batch_6" "batch_7" "batch_8" "batch_9")
#declare -a num_labeled=("50" "70"  "30" "90")
declare -a num_labeled=("10" "15" "20")
declare  -a epochs=("50")
#5 is      true
declare -a balanced_flag=("5" "-1")
declare -a lambda=("200")
declare -a mode=("ssdl" "partial_supervised")
for ((m=0;m<${#mode[@]};++m)); do
  for ((k=0;k<${#balanced_flag[@]};++k)); do
    for ((i=0;i<${#num_labeled[@]};++i)); do
      for ((j=0;j<${#batches[@]};++j)); do
        python ../MixMatch_OOD_main_balance_control_COVID.py --K_transforms 2 --T_sharpening 0.25 --alpha_mix 0.75 --balanced ${balanced_flag[k]} --batch_size 10 --epochs ${epochs[0]} --lambda_unsupervised ${lambda[0]} --log_folder logs_IEEE_Trans/logs_COVID_RSNA_${num_labeled[i]}_unlabeled_${num_unlabeled[i]}_balanced_${balanced_flag[k]} --lr 0.00002 --mode ${mode[m]} --model 'wide_resnet' --norm_stats 'Covid_cr' --num_classes 2 --number_labeled ${num_labeled[i]} --path_labeled  /media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_CR_30_val/${batches[j]}  --results_file_name stats_${mode[m]}_supervised_COVID_${num_labeled[i]}_unlabeled_${num_unlabeled[i]}_BALANCED_${balanced_flag[k]}_20_80_CR_ROC.csv --size_image 105 --weight_decay 0.0001 --desired_labeled_classes_dist "0.2, 0.8"
        python ../MixMatch_OOD_main_balance_control_COVID.py --K_transforms 2 --T_sharpening 0.25 --alpha_mix 0.75 --balanced ${balanced_flag[k]} --batch_size 10 --epochs ${epochs[0]} --lambda_unsupervised ${lambda[0]} --log_folder logs_IEEE_Trans/logs_COVID_RSNA_${num_labeled[i]}_unlabeled_${num_unlabeled[i]}_balanced_${balanced_flag[k]} --lr 0.00002 --mode ${mode[m]} --model 'wide_resnet' --norm_stats 'Covid_cr' --num_classes 2 --number_labeled ${num_labeled[i]} --path_labeled  /media/Data/saul/Datasets/Covid19/Dataset/batches_labeled_undersampled_in_dist_BINARY_CR_30_val/${batches[j]}  --results_file_name stats_${mode[m]}_supervised_COVID_${num_labeled[i]}_unlabeled_${num_unlabeled[i]}_BALANCED_${balanced_flag[k]}_30_70_CR_ROC.csv --size_image 105 --weight_decay 0.0001 --desired_labeled_classes_dist "0.3, 0.7"

      done
    done
  done
done
