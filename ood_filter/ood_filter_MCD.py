#OOD filter using softmax
from PIL import Image as Pili
import torch
from fastai.vision import *
from torchvision import models, transforms
import metrics as metrics
import shutil
torch.set_printoptions(threshold=10_000)

BATCH_SIZE = 4

class OOD_filter_MCD:
    def __init__(self, model_name = "densenet",  num_classes = 2):
        """
        OOD filter constructor
        :param model_name: name of the model to get the feature space from, pretrained with imagenet, wideresnet and densenet have been tested so far
        """
        self.model_name = model_name
        #list of scores, filepaths and labels of unlabeled data processed
        self.scores = []
        self.file_paths = []
        self.labels = []
        self.file_names = []
        self.info = []
        self.num_classes = num_classes

    def save_filtering_report(self, path_reports_ood, num_batch_data):
        """
        Saves the filtering report in a csv file
        :param path_reports_ood: where to save the ood report
        :param num_batch_data: batch number
        """
        # save filtering report
        print("file_names lenght ", len(self.file_names))
        print("scores ", len(self.scores))
        dict_csv = {'File_names': self.file_names,
                    'Scores': self.scores, "Info": self.info}
        dataframe = pd.DataFrame(dict_csv, columns=['File_names', 'Scores', 'Info'])
        dataframe.to_csv(path_reports_ood + 'scores_files_batch' + str(num_batch_data) + '.csv', index=False,
                         header=True)
        print(dataframe)


    def databunch_to_tensor(self, databunch1):
        """
        Convert the databunch to tensor set
        :param databunch1: databunch to convert
        :return: converted tensor
        """
        # tensor of tensor
        tensor_bunch = torch.zeros(len(databunch1.train_ds), databunch1.train_ds[0][0].shape[0],
                                   databunch1.train_ds[0][0].shape[1], databunch1.train_ds[0][0].shape[2], device="cuda:0")
        for i in range(0, len(databunch1.train_ds)):
            # print(databunch1.train_ds[i][0].data.shape)
            tensor_bunch[i, :, :, :] = databunch1.train_ds[i][0].data.to(device="cuda:0")

        return tensor_bunch


    def copy_filtered_observations(self, dir_root, percent_to_filter):
        """
        Copy filtered observations applying the thresholds
        :param dir_root: directory where to copy the filtered data
        :param   percent_to_filter: percent of observations to keep
        :return:
        """

        thresh = self.get_threshold(percent_to_filter)
        print("Threshold ", thresh)
        print("Percent to threshold: ", percent_to_filter)
        num_selected = 0
        # store info about the observation filtering
        self.info = [""] * len(self.scores)
        # only filter training
        for i in range(0, len(self.scores)):
            # The lower the MCD, the better
            if (self.scores[i] < thresh and "test" not in str(self.file_paths[i])):
                num_selected += 1
                rand_class = random.randint(0, self.num_classes - 1)
                path_dest = dir_root + "/train/" + str(rand_class) + "/"
                path_origin = self.file_paths[i]

                try:
                    os.makedirs(path_dest)
                except:
                    a = 0
                file_name = os.path.basename(self.file_paths[i])
                # print("File to copy", path_origin)
                # print("Path to copy", path_dest + file_name)
                shutil.copyfile(path_origin, path_dest + file_name)
                self.info[i] = "Copied, is training observation higher than thresh " + str(thresh)

            if ("test" in str(self.file_paths[i])):
                path_dest = dir_root + "/test/" + str(self.labels[i]) + "/"
                path_origin = self.file_paths[i]
                try:
                    os.makedirs(path_dest)
                except:
                    a = 0
                file_name = os.path.basename(self.file_paths[i])
                # print("File to copy", path_origin)
                # print("Path to copy", path_dest + file_name)
                shutil.copyfile(path_origin, path_dest + file_name)
                self.info[i] = "Copied, is test observation"
        print("Number of unlabeled observations preserved: ", num_selected)


    def get_threshold(self, percent_to_filter):
        """
        Get the threshold according to the list of observations and the percent of data to filter
        :param percent_to_filter: value from 0 to 1
        :return: the threshold
        """
        new_scores_no_validation = []
        for i in range(0, len(self.scores)):
            if("test" not in str(self.file_paths[i])):
                new_scores_no_validation += [self.scores[i]]

        #MCD the lower the better, sorting must be ascending
        new_scores_no_validation.sort()
        num_to_filter = int(percent_to_filter * len(new_scores_no_validation))
        threshold = new_scores_no_validation[num_to_filter]
        return threshold


    def pil2fast(self, img, im_size = 110):
        data_transform = transforms.Compose(
            [transforms.Resize((im_size, im_size)), transforms.ToTensor()])
        return Image(data_transform(img))

    """
    Get file names in path
    """
    def get_file_names_in_path(self, path):
        print("getting file names in path ", path)
        files_list = []
        for path, subdirs, files in os.walk(path):
            for name in files:
                file_path = os.path.join(path, name)
                files_list += [file_path]
        return files_list


    """
    Softmax calculator
    """
    def get_softmax_fastai(self, learner, input_image, is_ssdl = False):
        """

        :param learner:
        :param input_image:
        :param is_ssdl:
        :return:
        """

        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout = False)
        if (is_ssdl):
            model_output = nn.functional.softmax(model_output, dim=0)
        val, index = torch.max(model_output, 0)
        score_predicted = model_output[index]
        return tensor_class.item(), score_predicted

    def get_mc_dropout_fastai_all_classes(self, learner, input_image, max_its=100, is_ssdl=False):
        cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
        num_classes = model_output.shape[0]
        outputs = torch.zeros(max_its, num_classes)
        preds_class = []
        outputs_best = []
        for i in range(max_its):
            cat_tensor, tensor_class, model_output = learner.predict(input_image, with_dropout=True)
            # print("Model output before")
            # print(model_output)
            if (is_ssdl):
                model_output = nn.functional.softmax(model_output, dim=0)
            # print("Model output after")
            # print(model_output)
            pred_class_current = tensor_class.item()
            preds_class.append(pred_class_current)
            # for each class
            outputs[i, :] = model_output
            outputs_best += [model_output[pred_class_current].item()]


        std_all_classes = torch.std(outputs, 0)
        stds_all_sum = torch.sum(std_all_classes)
        pred_class_median = np.median(np.array(preds_class))
        std_best_class = np.std(np.array(outputs_best))
        print("pred class median ", pred_class_median)
        return outputs, pred_class_median, std_best_class, stds_all_sum

    def calculate_uncertainty_MCD_fastai_images(self, fastai_model, img_path, is_ssdl = True, im_size = 110):
        """

        :param fastai_model:
        :param img_path:
        :param is_ssdl:
        :param class_label:
        :param im_size:
        :return:
        """

        scores = []
        labels = []
        file_names = []
        file_paths = []
        tmp_label = 0
        list_images = self.get_file_names_in_path(img_path)
        print("total of images ", len(list_images))
        for i in range(0, len(list_images)):
            complete_path = list_images[i]
            file_paths += [complete_path]
            file_names += [list_images[i]]
            image_pil = Pili.open(complete_path).convert('RGB')
            image_fastai = self.pil2fast(image_pil, im_size =im_size)
            #use output with no dropout for pred class
            outputs, pred_class_median, std_best_class, stds_all_sum = self.get_mc_dropout_fastai_all_classes(fastai_model, image_fastai)
            #use the best class for this
            #saves the score
            scores += [std_best_class]
            #pick any folder for destination, anyways this information is not used in Mixmatch training
            labels += [tmp_label]
            if(tmp_label == 0): tmp_label = 1
            else: tmp_label = 0


        self.scores = scores
        self.labels = labels
        self.file_names = file_names
        self.file_paths = file_paths



    def run_filter(self, path_bunch1, path_bunch2, ood_perc=100, num_unlabeled=3000,  num_batches=10, size_image=120, batch_size_p=BATCH_SIZE, dir_filtered_root = "/media/Data/saul/Datasets/Covid19/Dataset/OOD_COVID19/OOD_FILTERED/batch_", ood_thresh = 0.8, path_reports_ood = "/reports_ood_softmax/", model_name = "densenet" ):
        """
        :param path_bunch1: path for the first data bunch, labeled data
        :param path_bunch2: unlabeled data
        :param ood_perc: percentage of data ood in the unlabeled dataset
        :param num_unlabeled: number of unlabeled observations in the unlabeled dataset
        :param name_ood_dataset: name of the unlabeled dataset
        :param num_batches: Number of batches of the unlabeled dataset to filter
        :param size_image: input image dimensions for the feature extractor
        :param batch_size_p: batch size
        :param dir_filtered_root: path for the filtered data to be stored
        :param ood_thresh: ood threshold to apply
        :param path_reports_ood: path for the ood filtering reports
        :return:
        """
        epochs_train_model = 50

        if(model_name == "densenet"):
            model_fastai = models.densenet121
            print("Creating densenet with num_classes ", self.num_classes)
        if(model_name == "alexnet"):
            model_fastai = models.alexnet
            print("Creating alexnet with num_classes ", self.num_classes)

        global key
        key = "pdf"
        print("Filtering OOD data for dataset at: ", path_bunch2)
        for num_batch_data in range(0, num_batches):
            # load pre-trained model, CORRECTION
            # model = models.alexnet(pretrained=True)

            print("Processing batch of labeled and unlabeled data: ", num_batch_data)
            # paths of data for all batches
            # DEBUG INCLUDE TRAIN
            path_labeled = path_bunch1 + "/batch_" + str(num_batch_data)
            path_unlabeled = path_bunch2 + str(num_batch_data) + "/batch_" + str(num_batch_data) + "_num_unlabeled_" + str(
                num_unlabeled) + "_ood_perc_" + str(ood_perc)+ "/train/"
            print("path labeled ", path_labeled)
            print("path unlabeled ", path_unlabeled)
            # get the dataset readers
            #  S_l
            databunch_labeled = (ImageList.from_folder(path_labeled)
                                 .split_none()
                                 .label_from_folder()
                                 .transform(size=size_image)
                                 .databunch())
            #et tensor bunches
            tensorbunch_labeled = self.databunch_to_tensor(databunch_labeled)
            num_obs_labeled = tensorbunch_labeled.shape[0]
            print("Number of  labeled observations in batch: ", num_obs_labeled)
            #train the model to use for the softmax
            learner = cnn_learner(data=databunch_labeled, base_arch=model_fastai, metrics=[accuracy])
            # train the model
            learner.fit_one_cycle(epochs_train_model, max_lr=0.00002)
            list_paths_classes_test = []

            """for i in range(0, num_classes):
                # CHANGE!! DEBUGGING!!
                print("Using test data in the labeled dataset, with paths:")
                print(path_labeled + "/test/" + str(i) + "/")
                list_paths_classes_test += [path_labeled + "/test/" + str(i) + "/"]
            list_metrics, list_metrics_names = metrics.get_metrics_multi_class(learner, list_paths_classes_test, im_size=32,
                                            name_report_roc= "_roc.csv")"""
            #print(list_metrics_names)
            #print(list_metrics)
            #calculate the MCD for the images and use it as score (atributes are updated here)
            self.calculate_uncertainty_MCD_fastai_images(learner, path_unlabeled, is_ssdl=False, im_size=size_image)
            # filter out the data and copy it
            dir_filtered = dir_filtered_root + "/batch_" + str(num_batch_data) + "/batch_" + str(
                num_batch_data) + "_num_unlabeled_" + str(num_unlabeled) + "_ood_perc_" + str(ood_perc) + "/"
            self.copy_filtered_observations(dir_filtered, ood_thresh)
            self.save_filtering_report(path_reports_ood, num_batch_data)





def simple_test_fastai():
    size_image = 105
    path_labeled = "E:/GoogleDrive/DATASETS_TEMP/batches_labeled_undersampled_in_dist_BINARY_CR_30_val/batch_0/"
    #  S_l
    databunch_labeled = (ImageList.from_folder(path_labeled)
                         .split_none()
                         .label_from_folder()
                         .transform(size=size_image)
                         .databunch())
    model_fastai = models.densenet121
    learner = cnn_learner(data=databunch_labeled, base_arch=model_fastai, metrics=[accuracy])
    learner.fit_one_cycle(50, max_lr=0.00002)

