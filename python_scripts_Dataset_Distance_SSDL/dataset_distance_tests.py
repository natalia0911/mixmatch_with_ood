import torch
import torchvision.models as models
from fastai.vision import *
import pingouin as pg
from fastai.callbacks import CSVLogger
from numbers import Integral
import torch
import logging
import sys
from torchvision.utils import save_image
import numpy as np
import pandas as pd
import scipy
from PIL import Image
import torchvision.models.vgg as models2
import torchvision.models as models3


from scipy.stats import entropy
from scipy.spatial import distance
# from utilities.InBreastDataset import InBreastDataset
import matplotlib
import matplotlib.pyplot as plt

import torchvision.transforms as transforms
import torchvision
from scipy.stats import mannwhitneyu
sys.path.insert(0, '../dataset_distance_measurer/')
from dataset_distance_measurer import *


def run_tests_minowski(p = 1, ood_perc = 50):
    print("Calculating distance for  IMAGENET TINY dataset")
    dataset_distance_tester(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_IMAGENET/batch_",
        ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="Imagenet" + str(p), num_batches=10, p=p)

    print("Calculating distance for  Gaussian dataset")
    dataset_distance_tester(
    path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
    path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_Gaussian/batch_",
    ood_perc=ood_perc,
    num_unlabeled=3000, name_ood_dataset="Gaussian", num_batches=10, p=p)
    #salt and pepper
    print("Calculating  distance for Salt and pepper dataset")
    dataset_distance_tester(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_Gaussian/batch_",
        ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="SaltAndPepper", num_batches=10, p = p)
    #print("Calculating distance for  half dataset")
    #dataset_distance_tester(name_ood_dataset="indist_p" + str(p), p=1, ood_perc=ood_perc)

    print("Calculating distance for SVHN dataset")
    dataset_distance_tester(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_SVHN/batch_", ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="SVHN" + str(p), num_batches=10, p=p)







def run_tests_pdf(distance_str = "js", ood_perc = 50):
    """

    :param distance: distance_str
    :return:
    """
    print("DISTANCE STR: ", distance_str)
    if(distance_str == "js"):
        distance_func = distance.jensenshannon
    elif(distance_str == "cosine"):
        distance_func = distance.cosine


    print("Calculating " + distance_str  + " distance for Gaussian dataset")
    dataset_distance_tester_pdf(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_Gaussian/batch_", ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="Gaussian_pdf_" + distance_str, num_batches=10, distance_func = distance_func)
    print("Calculating  " + distance_str + " distance for Salt and pepper dataset")
    dataset_distance_tester_pdf(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_SaltAndPepper/batch_",
        ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="SaltAndPepper_pdf_" + distance_str, num_batches=10,
        distance_func=distance_func)
    print("Calculating " + distance_str + " distance for SVHN dataset")


    dataset_distance_tester_pdf(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_SVHN/batch_", ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="SVHN__pdf_" + distance_str, num_batches=10, distance_func=distance_func)

    print("Calculating " + distance_str + " distance for IMAGENET dataset")
    dataset_distance_tester_pdf(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_IMAGENET/batch_",
        ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="IMAGENET_pdf_" + distance_str, num_batches=10,
        distance_func=distance_func)

    print("Calculating " + distance_str + " distance for HALF dataset")
    dataset_distance_tester_pdf(
        path_bunch1="/media/Data/saul/Datasets/MNIST_medium_complete/batches_labeled_in_dist/batch_",
        path_bunch2="/media/Data/saul/Datasets/MNIST_medium_complete/batches_unlabeled_out_dist/batch_", ood_perc=ood_perc,
        num_unlabeled=3000, name_ood_dataset="half_pdf_" + distance_str, num_batches=10, distance_func = distance_func)


#run_tests_minowski(p = 1, ood_perc = 50)